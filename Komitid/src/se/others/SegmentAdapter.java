package se.others;

import java.text.SimpleDateFormat;
import java.util.ArrayList;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import se.entities.Segment;
import se.komitid.R;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * An adapter that presents segments in an listview.
 * @author Simon Cedergren AB1539 <simon@tuxflux.se>
 */

public class SegmentAdapter extends ArrayAdapter<Segment> {
	private Context context;
	private int layoutResourceId;
	private ArrayList<Segment> data = null;
	private final String tag = "Komitid | SegmentAdapter";
	private SimpleDateFormat dateFormatter = new SimpleDateFormat("MM-dd HH:mm");

	
	public SegmentAdapter(Context context, int resource, ArrayList<Segment> data) {
		super(context, resource, data);
		this.layoutResourceId = resource;
		this.context = context;
		this.data = data;
	}

	@Override
	public View getView(int position, View view, ViewGroup parent) {
		LayoutInflater inflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View rowView = inflater.inflate(R.layout.row_checkpoint, parent, false);
		Segment segment = data.get(position);
		
		Log.i(tag, "segment="+segment);

		TextView tv_depName = (TextView)rowView.findViewById(R.id.tv_depName);
		TextView tv_depTime = (TextView)rowView.findViewById(R.id.tv_depTime);
		TextView tv_arrName = (TextView)rowView.findViewById(R.id.tv_arrName);
		TextView tv_arrTime = (TextView)rowView.findViewById(R.id.tv_arrTime);
		ImageView iv_icon = (ImageView)rowView.findViewById(R.id.iv_transportMethod);
		if (segment.getTransportMethod().contains("Gång")) {
			iv_icon.setImageResource(R.drawable.ic_running);
		}else if (segment.getTransportMethod().toLowerCase().contains("buss")) {
			iv_icon.setImageResource(R.drawable.ic_buss);
		}else if (segment.getTransportMethod().toLowerCase().contains("tåg")) {
			iv_icon.setImageResource(R.drawable.ic_train);
		}else if (segment.getTransportMethod().toLowerCase().contains("båt")) {
			iv_icon.setImageResource(R.drawable.ic_boat);
		}else if (segment.getTransportMethod().toLowerCase().contains("färja")) {
			iv_icon.setImageResource(R.drawable.ic_boat);
		}else {
			iv_icon.setImageResource(R.drawable.ic_unknown);
		}

		// Converts from milliseconds to human readable text.
		DateTimeFormatter dtfTime = DateTimeFormat.forPattern("HH:mm");
		DateTimeFormatter dtfDate = DateTimeFormat.forPattern("dd/MM");
		DateTime depTime = new DateTime(segment.getDepTime());
		DateTime arrTime = new DateTime(segment.getArrTime());
		String depTimeString = depTime.toString(dtfTime);
		String arrTimeString = arrTime.toString(dtfTime);
		String depDateString = depTime.toString(dtfDate);
		String arrDateString = arrTime.toString(dtfDate);
		String time = rowView.getResources().getString(R.string.time);
		String date = rowView.getResources().getString(R.string.date);
		String with = rowView.getResources().getString(R.string.with);
		
		// Show transport method if there is any.
		String showInDepName = segment.getDepName();
		if (!(segment.getNumber().equals("") || segment.getTransportMethod().equals(""))) {
			showInDepName = segment.getDepName() + " " +with + " " + segment.getTransportMethod().toLowerCase();
			if (!segment.getNumber().equals("Ventr"))
				showInDepName += " " + segment.getNumber();
		}
		
		tv_depName.setText(showInDepName);
		tv_depTime.setText(time + " " + depTimeString + "\n" + date + " " + depDateString);
		tv_arrName.setText(segment.getArrName());
		tv_arrTime.setText(time + " " + arrTimeString + "\n" + date + " " + arrDateString);
		
		return rowView;
	}
}
