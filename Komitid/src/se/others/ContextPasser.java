package se.others;

import android.content.Context;

/**
 * A clever class that passes the MainActivity context!
 * 
 * @author Simon Cedergren
 */
public class ContextPasser {
	private static Context localContext;

	public static Context getLocalContext() {
		return localContext;
	}

	public static void setLocalContext(Context localContext) {
		ContextPasser.localContext = localContext;
	}
}