package se.controllers;

import java.util.ArrayList;

import se.entities.Segment;
import se.others.ContextPasser;
import se.storage.DBAdapter;
import android.content.Context;
import android.database.Cursor;

/**
 * Controller for FragTimeTable
 * @author Simon Cedergren <simon@tuxflux.se>
 */

public class CtrlrTimeTable {
	private Context context = ContextPasser.getLocalContext();
	private DBAdapter db = new DBAdapter(context);
	private final String tag = "Komitid | CtrlrTimeTables";

	/**
	 * Returns all the segments from database as an arraylist
	 * @return Returns all the segments from database as an arraylist
	 */
	public ArrayList<Segment> getAllSegments() {
		ArrayList<Segment> segments = new ArrayList<Segment>();
		db.open();
		Cursor departures = db.getAllDepartures();
		Cursor arrivals = db.getAllArrivls();
		while (departures.moveToNext()) {
			arrivals.moveToNext();
			Segment s = new Segment();
			s.setSegment(departures.getInt(0));
			s.setDepLat(departures.getDouble(1));
			s.setDepLng(departures.getDouble(2));
			s.setDepName(departures.getString(3));
			s.setDepTime(departures.getLong(4));
			s.setCarrier_name(departures.getString(10));
			s.setTransport_id(departures.getString(8));
			s.setCarrier_url(departures.getString(9));
			s.setNumber(departures.getString(7));
			s.setArrLat(arrivals.getDouble(1));
			s.setArrLng(arrivals.getDouble(2));
			s.setArrName(arrivals.getString(3));
			s.setArrTime(arrivals.getLong(4));
			s.setTransportMethod(departures.getString(6));
			s.setDistance(departures.getInt(5));
			segments.add(s);
		}
		return segments;
	}
}
