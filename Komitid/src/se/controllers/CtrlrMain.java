package se.controllers;

import se.others.ContextPasser;
import android.content.Context;
import android.os.Build;
import android.provider.Settings;

/**
 * The controller class for MainActivity. Mainly used for checking if location
 * services are active on the device.
 * 
 * @author Simon Cedergren <simon@tuxflux.se>
 * 
 */
public class CtrlrMain {
	private Context c = ContextPasser.getLocalContext();

	/**
	 * Checks if location is active on device Note: On devices with API >= 19,
	 * this check will return true, since the way of getting location status has
	 * changed and is broken.
	 * 
	 * @return true if any location service is active
	 */
	public boolean isLocationActivated() {
		if (Build.VERSION.SDK_INT < 19) {
			String locationProvider = Settings.Secure.getString(c.getContentResolver(), Settings.Secure.LOCATION_PROVIDERS_ALLOWED);
			if (locationProvider.equals("")) {
				return false;
			} else {
				return true;
			}
		}
		return true;
	}
}
