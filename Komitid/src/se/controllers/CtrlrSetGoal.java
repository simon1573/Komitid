package se.controllers;

import se.apihandlers.TrafiklabHandler;
import se.komitid.R;
import se.others.AlarmHandler;
import se.others.ContextPasser;
import se.storage.DBAdapter;
import se.storage.SharedPrefsHandler;
import android.app.ProgressDialog;
import android.content.Context;
import android.database.Cursor;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

/**
 * The controller for FragSetGoal. This class handles the functionality for
 * creating a trip query, and setting the alarm.
 * 
 * @author Fredrik Andersson <cfredrikandersson@gmail.com>
 * 
 */
public class CtrlrSetGoal {

	private final String tag = "Komitid | CtrlrSetGoal";

	private TrafiklabHandler trafiklab;
	private DBAdapter db;
	private Context context;

	/**
	 * Basic constructor that initializes the required components.
	 */
	public CtrlrSetGoal() {
		this.context = ContextPasser.getLocalContext();
		this.trafiklab = new TrafiklabHandler();
		this.db = new DBAdapter(context);
	}

	/**
	 * Utilizes the data stored in SharedPreferences to create a query and sends
	 * it via TrafiklabHandler using an AsyncTask.
	 */
	public void sendQueryFromPrefs() {
		SendQueryFromPrefs sendQuery = new SendQueryFromPrefs();
		sendQuery.execute();
	}

	/**
	 * An AsyncTask responsible for using data stored in SharedPreferences to
	 * create a query and then send it using TrafiklabHandler. If the query is
	 * successful, the alarm counter is started.
	 * 
	 * @author Fredrik Andersson <cfredrikandersson@gmail.com>
	 * 
	 */
	private class SendQueryFromPrefs extends AsyncTask<String, Void, Boolean> {

		private ProgressDialog progressDiag;

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			progressDiag = new ProgressDialog(ContextPasser.getLocalContext());
			progressDiag.setMessage(context.getString(R.string.getting_trip_data));
			progressDiag.show();
		}

		@Override
		protected Boolean doInBackground(String... params) {
			String cityFrom = context.getString(R.string.start_position);
			String cityTo = context.getString(R.string.end_position);
			double arrLat = SharedPrefsHandler.getDestLatitude();
			double arrLng = SharedPrefsHandler.getDestLongitude();
			double depLat = SharedPrefsHandler.getStartLatitude();
			double depLng = SharedPrefsHandler.getStartLongitude();
			long arrTime = SharedPrefsHandler.getArrivalTime();
			boolean includeTrains = SharedPrefsHandler.getTrainEnabled();
			boolean includeBuses = SharedPrefsHandler.getBusEnabled();
			boolean includeBoats = SharedPrefsHandler.getBoatEnabled();
			double walkingSpeed = (SharedPrefsHandler.getWalkingSpeed() / 3600) * 1000;

			trafiklab.prepareTripQuery(cityFrom, cityTo, arrLat, arrLng, depLat, depLng, arrTime, includeTrains, includeBuses, includeBoats,
					walkingSpeed);

			return trafiklab.sendQuery();
		}

		@Override
		protected void onPostExecute(Boolean isTripPossible) {
			super.onPostExecute(isTripPossible);

			progressDiag.dismiss();

			if (isTripPossible) {
				AlarmHandler alarm = new AlarmHandler();
				long alarmTime = 0;

				// If debug mode is enabled the alarm will always go off 10 sec
				// from when it was set.
				if (SharedPrefsHandler.getDebugMode()) {
					alarmTime = System.currentTimeMillis() + 10000;

				} else {
					alarmTime = calculateAlarmTime();
				}

				alarm.setAlarm(context, alarmTime);
				SharedPrefsHandler.setAlarmTime(alarmTime);
				SharedPrefsHandler.setAppState(SharedPrefsHandler.STATE_ALARM);
				Log.i(tag, "Alarmtime: " + alarmTime);
				Toast.makeText(ContextPasser.getLocalContext(), R.string.timer_started, Toast.LENGTH_SHORT).show();
				Log.i(tag, "Alarm set!");
			} else {
				trafiklab.showTripErrorDialog();
			}
		}

		/**
		 * Calculates the time at which the alarm should go off, based on the
		 * time of the first departure in the DB, and the chosen amount of
		 * preparation time.
		 * 
		 * @return The time at which the alarm should go off.
		 */
		private long calculateAlarmTime() {
			db.open();
			Cursor depart = db.getFirstDepartureTime();
			depart.moveToFirst();
			long departure = depart.getLong(0);
			db.close();
			long prepTime = SharedPrefsHandler.getPreparationTime();
			Log.i(tag, "Calculated alarm time: " + (departure - prepTime));
			return departure - prepTime;
		}
	}

	/**
	 * Checks if a trip is currently active.
	 * 
	 * @return 'true' if a trip is active, otherwise 'false'.
	 */
	public boolean checkForRunningTrip() {
		if (SharedPrefsHandler.tripIsActive())
			return true;
		else if (SharedPrefsHandler.getAlarmTime() != 0)
			return true;
		else
			return false;
	}

}
