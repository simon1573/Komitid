package se.controllers;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import se.fragments.FragTime;
import se.komitid.MainActivity;
import se.komitid.R;
import se.others.ContextPasser;
import se.storage.DBAdapter;
import se.storage.SharedPrefsHandler;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.database.Cursor;
import android.media.MediaPlayer;
import android.os.Vibrator;

/**
 * The controller responsible for displaying the correct time in FragTime, based
 * on the current state of the app.
 * 
 * @author Fredrik Andersson <cfredrikandersson@gmail.com>
 * @author Simon Cedergren <simon@tuxflux.se>
 */
public class CtrlrTime {
	private final String tag = "Komitid | CtrlrAlarm";

	private boolean running = false;
	private FragTime FragTime;
	private MainActivity main;
	private DateTimeFormatter dtfTime;
	private DateTime tempTime;
	private MediaPlayer mediaPlayer;
	private DBAdapter db;
	private Context context = ContextPasser.getLocalContext();

	private long alarmTime;
	private long prepTime;
	private long nextDepTime;
	private int currentSegment;

	private String travelMsg = context.getString(R.string.next_departure_in);

	/**
	 * Basic constructor.
	 * 
	 * @param FragTime
	 *            A reference to the fragment which this controller should be
	 *            associated with.
	 * @param main
	 *            A reference to MainActivity.
	 */
	public CtrlrTime(FragTime FragTime, MainActivity main) {
		this.FragTime = FragTime;
		this.db = new DBAdapter(context);
		this.dtfTime = DateTimeFormat.forPattern("HH:mm:ss");
		this.main = main;
	}

	/**
	 * Retrieves the trip data from SharedPreferences and the database, and
	 * stores it in the instance variables.
	 */
	private void prepareTripData() {
		alarmTime = SharedPrefsHandler.getAlarmTime();
		prepTime = SharedPrefsHandler.getAlarmTime() + SharedPrefsHandler.getPreparationTime();
		currentSegment = SharedPrefsHandler.getCurrentSegment();
		nextDepTime = getNextDepTime();
	}

	/**
	 * Starts the time thread.
	 */
	public void startTimeThread() {
		prepareTripData();
		Thread thread = new Thread(new AlarmThread());
		running = true;
		thread.start();
	}

	/**
	 * Returns the currently set alarm time in the form of a String (HH:MM:SS)
	 * 
	 * @return The currently set alarm time in the form of a String (HH:MM:SS).
	 */
	public String getAlarmTime() {
		return new DateTime(SharedPrefsHandler.getAlarmTime()).toString(dtfTime);
	}

	/**
	 * Stops the time thread.
	 */
	public void stopTimeThread() {
		running = false;
	}

	/**
	 * Calculates the time remaining until the alarm will ring, and returns it
	 * in the form of a String.
	 * 
	 * @return The time remaining until the alarm will ring in the form of a
	 *         String (HH:MM:SS).
	 */
	public String alarmRemaining() {

		if (alarmTime < System.currentTimeMillis()) {
			SharedPrefsHandler.setAppState(SharedPrefsHandler.STATE_PREP);
			return "00:00:00";
		} else {
			tempTime = new DateTime(alarmTime - System.currentTimeMillis());
			tempTime = tempTime.minusHours(1);
			return tempTime.toString(dtfTime);
		}
	}

	/**
	 * Calculates the remaining preparation time and returns it in the form of a
	 * String.
	 * 
	 * @return The remaning preparation time in the form of a Sting (HH:MM:SS).
	 */
	public String prepRemaining() {

		if (prepTime < System.currentTimeMillis()) {
			SharedPrefsHandler.setAppState(SharedPrefsHandler.STATE_TRAVELING);
			return "00:00:00";
		} else {
			tempTime = new DateTime(prepTime - System.currentTimeMillis());
			tempTime = tempTime.minusHours(1);
			return tempTime.toString(dtfTime);
		}
	}

	/**
	 * Calculates the time remaining until the next departure (or the last
	 * arrival, if all departures have already passed) and returns it in the
	 * form of a String.
	 * 
	 * @return The time remaining until the next departure (or the last arrival)
	 *         in the form of a String (HH:MM:SS).
	 */
	public String nextDepRemaining() {
		if (nextDepTime < System.currentTimeMillis()) {
			if (currentSegment < getNumberOfSegments() - 1) {
				currentSegment++;
				nextDepTime = getNextDepTime();
				travelMsg = context.getString(R.string.next_departure_in);
			} else {
				nextDepTime = getLastArrTime();
				travelMsg = context.getString(R.string.reach_destination_in);
			}
		}

		if (nextDepTime - System.currentTimeMillis() > 0) {
			tempTime = new DateTime(nextDepTime - System.currentTimeMillis());
			tempTime = tempTime.minusHours(1);
			return tempTime.toString(dtfTime);
		} else {
			return "00:00:00";
		}
	}

	/**
	 * Returns the number of trip segments stored in the database.
	 * 
	 * @return The number of trip segments stored in the database.
	 */
	private int getNumberOfSegments() {
		int counter = 0;

		db.open();
		Cursor c = db.getAllDepartures();
		counter = c.getCount();
		db.close();

		return counter;
	}

	/**
	 * The time of the next departure stored in the database.
	 * 
	 * @return The time of the next departure stored in the database.
	 */
	private long getNextDepTime() {
		long depTime = 0;
		db.open();
		Cursor nextDep = db.getDeparture(currentSegment);
		nextDep.moveToNext();
		depTime = nextDep.getLong(4);
		db.close();
		return depTime;
	}

	/**
	 * The time of the last arrival stored in the database.
	 * 
	 * @return The time of the last arrival stored in the database.
	 */
	private long getLastArrTime() {
		long lastArrTime = 0;
		db.open();
		Cursor arrTime = db.getAllArrivls();
		arrTime.moveToLast();
		lastArrTime = arrTime.getLong(4);
		db.close();
		return lastArrTime;
	}

	/**
	 * Checks if the alarm is currently ringing.
	 * 
	 * @return 'true' if the alarm is ringing, otherwise 'false'.
	 */
	public boolean isAlarmRinging() {
		if (SharedPrefsHandler.isAlarmRinging()) {
			SharedPrefsHandler.setAlarmRinging(false);
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Displays an AlertDialog, and plays an alarm sound. The sound is stopped
	 * when the dialog is closed.
	 * 
	 * @return An AlertDialog containing a message and playing an alarm sound.
	 */
	public AlertDialog showAlarmDialog() {
		mediaPlayer = MediaPlayer.create(context, R.raw.alarm);
		mediaPlayer.setLooping(true);
		mediaPlayer.start();
		startVibration();
		AlertDialog.Builder diagAlarm = new AlertDialog.Builder(context);
		diagAlarm.setTitle(R.string.app_name);
		diagAlarm.setMessage(R.string.alarm_ringing);
		diagAlarm.setPositiveButton("Ok", new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				mediaPlayer.stop();
				Vibrator vibrator = (Vibrator) context.getSystemService(Context.VIBRATOR_SERVICE);
				vibrator.cancel();
				dialog.dismiss();
			}
		});
		return diagAlarm.show();
	}

	/**
	 * Starts the internal vibration of the phone.
	 */
	private void startVibration() {
		Vibrator vibrator = (Vibrator) context.getSystemService(Context.VIBRATOR_SERVICE);
		long[] vib = new long[50];
		for (int i = 0; i < vib.length; i++) {
			vib[i] = i * 100;
		}
		vibrator.vibrate(vib, 1);
	}

	/**
	 * The thread responsible for passing the correct time info to the
	 * components in FragTime, based on the current app state.
	 * 
	 * @author Fredrik Andersson <cfredrikandersson@gmail.com>
	 * 
	 */
	private class AlarmThread implements Runnable {
		int appState = 0;

		@Override
		public void run() {
			while (running) {
				appState = SharedPrefsHandler.getAppState();
				main.runOnUiThread(new Runnable() {

					@Override
					public void run() {

						// Sets the label/counter on FragTime.
						if (FragTime.isInflated()) {

							switch (appState) {
							case SharedPrefsHandler.STATE_IDLE:
								FragTime.setLabel(context.getString(R.string.no_active_alarm));
								FragTime.setTime("");
								FragTime.setButtonVisible(false);
								break;
							case SharedPrefsHandler.STATE_ALARM:
								FragTime.setLabel(context.getString(R.string.alarm_will_ring_in));
								FragTime.setTime(alarmRemaining());
								FragTime.setButtonVisible(true);
								break;
							case SharedPrefsHandler.STATE_PREP:
								FragTime.setLabel(context.getString(R.string.you_must_go_in));
								FragTime.setTime(prepRemaining());
								FragTime.setButtonVisible(false);
								break;
							case SharedPrefsHandler.STATE_TRAVELING:
								FragTime.setLabel(travelMsg);
								FragTime.setTime(nextDepRemaining());
								FragTime.setButtonVisible(false);
								break;
							}

							if (isAlarmRinging()) {
								showAlarmDialog();
							}
						}
					}
				});

				if (!SharedPrefsHandler.tripIsActive()) {
					stopTimeThread();
					SharedPrefsHandler.setAppState(SharedPrefsHandler.STATE_IDLE);
					SharedPrefsHandler.restoreDefaultSettings();
					db.open();
					db.dropArrivals();
					db.dropDepartures();
					db.close();

					main.runOnUiThread(new Runnable() {

						@Override
						public void run() {
							FragTime.setLabel(context.getString(R.string.you_are));
							FragTime.setTime(context.getString(R.string.late));
						}
					});
				}

				try {
					Thread.sleep(200);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		}
	}
}
