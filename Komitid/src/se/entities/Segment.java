package se.entities;

/**
 * A class that represent a Segment.
 * @author Simon Cedergren <simon@tuxflux.se>
 */

public class Segment {
	
	private int segment, distance;
	private double depLat, depLng, arrLat, arrLng;
	private String depName, arrName, transportMethod, number, transport_id, carrier_url, carrier_name;
	private long depTime, arrTime;
	
	public int getSegment() {
		return segment;
	}
	
	public void setSegment(int segment) {
		this.segment = segment;
	}
	
	public int getDistance() {
		return distance;
	}
	
	public void setDistance(int distance) {
		this.distance = distance;
	}
	
	public double getDepLat() {
		return depLat;
	}
	
	public void setDepLat(double depLat) {
		this.depLat = depLat;
	}
	
	public double getDepLng() {
		return depLng;
	}
	
	public void setDepLng(double depLng) {
		this.depLng = depLng;
	}
	
	public double getArrLat() {
		return arrLat;
	}
	
	public void setArrLat(double arrLat) {
		this.arrLat = arrLat;
	}
	
	public double getArrLng() {
		return arrLng;
	}
	
	public void setArrLng(double arrLng) {
		this.arrLng = arrLng;
	}
	
	public String getDepName() {
		return depName;
	}
	
	public void setDepName(String depName) {
		this.depName = depName;
	}
	
	public String getArrName() {
		return arrName;
	}
	
	public void setArrName(String arrName) {
		this.arrName = arrName;
	}
	
	public String getTransportMethod() {
		return transportMethod;
	}
	
	public void setTransportMethod(String transportMethod) {
		this.transportMethod = transportMethod;
	}
	
	public long getDepTime() {
		return depTime;
	}
	
	public void setDepTime(long depTime) {
		this.depTime = depTime;
	}
	
	public long getArrTime() {
		return arrTime;
	}
	
	public void setArrTime(long arrTime) {
		this.arrTime = arrTime;
	}

	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	public String getTransport_id() {
		return transport_id;
	}

	public void setTransport_id(String transport_id) {
		this.transport_id = transport_id;
	}

	public String getCarrier_url() {
		return carrier_url;
	}

	public void setCarrier_url(String carrier_url) {
		this.carrier_url = carrier_url;
	}

	public String getCarrier_name() {
		return carrier_name;
	}

	public void setCarrier_name(String carrier_name) {
		this.carrier_name = carrier_name;
	}

	@Override
	public String toString() {
		return "Segment [segment=" + segment + ", distance=" + distance + ", depLat=" + depLat + ", depLng=" + depLng + ", arrLat=" + arrLat + ", arrLng=" + arrLng + ", depName=" + depName + ", arrName=" + arrName + ", transportMethod=" + transportMethod + ", number=" + number + ", transport_id=" + transport_id + ", carrier_url=" + carrier_url + ", carrier_name=" + carrier_name + ", depTime=" + depTime + ", arrTime=" + arrTime + "]";
	}
}
