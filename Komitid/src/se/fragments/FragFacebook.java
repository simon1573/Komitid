package se.fragments;

import java.util.Arrays;
import java.util.List;

import se.komitid.R;
import se.others.ContextPasser;
import se.storage.SharedPrefsHandler;
import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.facebook.Session;
import com.facebook.SessionState;
import com.facebook.UiLifecycleHelper;
import com.facebook.widget.LoginButton;

/**
 * Fragment that handles the Facebook login/logout.
 * 
 * @author Fredrik Andersson <cfredrikandersson@gmail.com>
 * 
 */
public class FragFacebook extends Fragment {

	private Context c = ContextPasser.getLocalContext();

	private static final String tag = "Komitid | FragFacebook";

	private UiLifecycleHelper uiHelper;

	private final List<String> pubPerm;

	/**
	 * Basic constructor.
	 */
	public FragFacebook() {
		pubPerm = Arrays.asList("publish_actions");
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		uiHelper = new UiLifecycleHelper(getActivity(), callback);
		uiHelper.onCreate(savedInstanceState);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.layout_frag_facebook, container, false);
		this.getActivity().getActionBar().setTitle(R.string.facebook);

		LoginButton authButton = (LoginButton) view.findViewById(R.id.btnFBLogin);
		authButton.setPublishPermissions(pubPerm);

		return view;
	}

	@Override
	public void onResume() {
		super.onResume();
		Session session = Session.getActiveSession();
		if (session != null && (session.isOpened() || session.isClosed())) {
			onSessionStateChange(session, session.getState(), null);
		}
		uiHelper.onResume();
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		uiHelper.onActivityResult(requestCode, resultCode, data);
	}

	@Override
	public void onPause() {
		super.onPause();
		uiHelper.onPause();
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		uiHelper.onDestroy();
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		uiHelper.onSaveInstanceState(outState);
	}

	/**
	 * Called if the state of the current Session is changed (login/logout).
	 * 
	 * @param session
	 *            The current Session
	 * @param state
	 *            The state of the Session
	 * @param exception
	 *            Exception that might have been thrown.
	 */
	private void onSessionStateChange(Session session, SessionState state, Exception exception) {
		if (state.isOpened()) {
			Log.i(tag, "User logged in.");
			SharedPrefsHandler.setFacebookLoginStatus(true);
		} else if (state.isClosed()) {
			Log.i(tag, "User logged out.");
			SharedPrefsHandler.setFacebookLoginStatus(false);
		}
	}

	/**
	 * Handles the changing of Session states.
	 */
	public Session.StatusCallback callback = new Session.StatusCallback() {
		@Override
		public void call(Session session, SessionState state, Exception exception) {
			onSessionStateChange(session, state, exception);

		}
	};
}
