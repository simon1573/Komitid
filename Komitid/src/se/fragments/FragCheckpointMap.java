package se.fragments;

import java.util.ArrayList;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import se.controllers.CtrlService;
import se.entities.Checkpoint;
import se.komitid.R;
import se.storage.DBAdapter;
import se.storage.SharedPrefsHandler;
import android.app.Dialog;
import android.app.Fragment;
import android.location.Criteria;
import android.location.Location;
import android.os.Bundle;
import android.util.Log;
import android.view.InflateException;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesClient;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.location.LocationClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.OnMapLongClickListener;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
/**
 * A class that lets the user see its checkpoints on a map
 * @author Simon Cedergren <simon@tuxflux.se>
 */
public class FragCheckpointMap extends Fragment implements LocationListener, GooglePlayServicesClient.ConnectionCallbacks, GooglePlayServicesClient.OnConnectionFailedListener {

	private final String tag = "Maps | FragMap";
	private View view;
	private GoogleMap map;
	private LocationRequest locationReq;
	private LocationClient locationClient;
	private double lat = -1, lng = -1;
	private boolean hasBeenCentered = false;
	private DBAdapter db;
	private CtrlService controller;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

		if (view != null) {
			ViewGroup parent = (ViewGroup) view.getParent();
			if (parent != null)
				parent.removeView(view);
		}
		try {
			this.view = inflater.inflate(R.layout.layout_map, container, false);
		} catch (InflateException e) {
			Log.e(tag, null, e);
		}
		db = new DBAdapter(getActivity());
		controller = new CtrlService(db);
		this.getActivity().getActionBar().setTitle(R.string.checkpoint_map);
		return view;
	}

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		menu.clear();
		inflater.inflate(R.menu.main, menu);
	}

	@Override
	public void onDestroyView() {
		super.onDestroyView();
		MapFragment f = (MapFragment) getFragmentManager().findFragmentById(R.id.map);
		if (f != null)
			getFragmentManager().beginTransaction().remove(f).commit();
	}

	@Override
	public void onResume() {
		initComponents();
		super.onResume();
	}

	@Override
	public void onPause() {
		super.onPause();
	}

	@Override
	public void onStop() {
		locationClient.disconnect();
		super.onStop();
	}

	/**
	 * Initilizes the components
	 */
	private void initComponents() {

		Criteria criteria = new Criteria();
		criteria.setAccuracy(Criteria.ACCURACY_FINE);
		map = ((MapFragment) getFragmentManager().findFragmentById(R.id.map)).getMap();
		locationClient = new LocationClient(getActivity(), this, this);
		map.setMapType(3); // Satellite view.
		// getActivity().getActionBar().setDiSharedPrefsHandlerlayHomeAsUpEnabled(false); //
		// This adds a back-arrow in action bar. Needs a back stack to work.

		setHasOptionsMenu(true);
		if (isServicesConnected()) {
			locationReq = LocationRequest.create();
			locationReq.setInterval(60000);
			locationReq.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
		}
		locationClient.connect();
		
		registerMapListener();
		addCheckpointsToMap();
	}

	@Override
	public void onLocationChanged(Location location) {
		lat = location.getLatitude();
		lng = location.getLongitude();

		if (!hasBeenCentered) {
			CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(new LatLng(lat, lng), 10);
			map.animateCamera(cameraUpdate);
			hasBeenCentered = true;
		}
	}

	@Override
	public void onConnectionFailed(ConnectionResult result) {
		Toast.makeText(getActivity(), getString(R.string.error_connecting_gps), Toast.LENGTH_SHORT).show();
	}

	@Override
	public void onConnected(Bundle connectionHint) {
		try {
			Thread.sleep(100); // TODO: Fulhack. This lets the device to finish
								// the connection.
			locationClient.requestLocationUpdates(locationReq, this);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Registers the marker-listerner
	 */
	private void registerMapListener() {
		map.setOnMapLongClickListener(new OnMapLongClickListener() {

			@Override
			public void onMapLongClick(LatLng loc) {
				map.clear();
				addCheckpointsToMap();
				double debugLat = loc.latitude;
				double debugLng = loc.longitude;
				SharedPrefsHandler.setDebugLat(debugLat);
				SharedPrefsHandler.setDebugLng(debugLng);
				MarkerOptions marker = new MarkerOptions().title("Debug location").position(new LatLng(debugLat, debugLng));
				map.addMarker(marker);
			}
		});
	}

	@Override
	public void onDisconnected() {
		Toast.makeText(getActivity(), getString(R.string.disconnected_from_gps), Toast.LENGTH_SHORT).show();
	}

	/**
	 * Check if connected to GCM
	 * @return true if connected to GCM
	 */
	private boolean isServicesConnected() {
		boolean isConnected = false;
		int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(getActivity());
		if (ConnectionResult.SUCCESS == resultCode) {
			isConnected = true;
		} else {
			Dialog errorDialog = GooglePlayServicesUtil.getErrorDialog(resultCode, getActivity(), 9000);
			if (errorDialog != null) {
				errorDialog.show();
			}
			isConnected = false;
		}
		return isConnected;
	}
	
	/**
	 * Adds a marker on every checkpoint
	 */
	private void addCheckpointsToMap() {
		ArrayList<Checkpoint> cps = controller.getAllCheckpoints();
		DateTimeFormatter dtfTime = DateTimeFormat.forPattern("HH:mm");
		String timeHint = getString(R.string.time);
		for (int i = 0; i < cps.size(); i++) {
			Checkpoint c = cps.get(i);
			DateTime tmpTime = new DateTime(c.getTime());
			String time = tmpTime.toString(dtfTime);
			Marker m = map.addMarker(new MarkerOptions()
		     .position(new LatLng(c.getLat(), c.getLng()))
		     .title(i +". "+ c.getName())
		     .snippet(timeHint+" "+time));
			m.showInfoWindow();
		}
	}
}
