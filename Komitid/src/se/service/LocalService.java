package se.service;

import se.controllers.CtrlService;
import se.entities.Checkpoint;
import se.komitid.R;
import se.storage.DBAdapter;
import se.storage.Logger;
import se.storage.SharedPrefsHandler;
import android.app.Activity;
import android.app.Dialog;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.Service;
import android.content.Intent;
import android.location.Criteria;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.os.Process;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesClient;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.location.LocationClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;

/**
 * A service that controls the flow of the application while its not visible.
 * 
 * @author Simon Cedergren <simon@tuxflux.se>
 */

public class LocalService extends Service implements LocationListener, GooglePlayServicesClient.ConnectionCallbacks,
        GooglePlayServicesClient.OnConnectionFailedListener {

	private final String tag = "Komitid | LocalService";

	private Looper looper;
	private ServiceHandler handler;
	private LocationRequest locationReq;
	private LocationClient locationClient;
	private DBAdapter db;
	private double lng = 0, lat = 0;
	private CtrlService controller;
	private Message msg;
	private NotificationManager notificationManager;
	private Notification.Builder notificationBuilder;
	private int notificationID = 001;

	private final class ServiceHandler extends Handler {
		public ServiceHandler(Looper looper) {
			super(looper);
		}

		@Override
		public void handleMessage(Message msg) {
			synchronized (this) {
				setMsg(msg); // Saves the msg so that the service can be stopped.
			}
		}
	}
	
	/**
	 * Setter for messages, used to quit the service
	 * @param msg - last message received to service
	 */
	public void setMsg(Message msg) {
		this.msg = msg;
	}

	/**
	 * Returns the last received message to the service, used to quit the service
	 * @return msg - the last received msg
	 */
	public Message getMsg() {
		return msg;
	}

	// Ran once, when the LocalService-object is created.
	@Override
	public void onCreate() {
		// Starts a separate thread for the service.
		HandlerThread thread = new HandlerThread("ServiceStartArgs", Process.THREAD_PRIORITY_FOREGROUND);
		thread.start();
		db = new DBAdapter(getApplicationContext());
		controller = new CtrlService(db);

		// Builds and prints the notification to keep the service alive while its needed
		notificationBuilder = new Notification.Builder(getApplicationContext()).setSmallIcon(R.drawable.ic_launcher)
		        .setDefaults(Notification.FLAG_FOREGROUND_SERVICE).setContentTitle(getString(R.string.app_name));
		notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
		notificationManager.notify(notificationID, notificationBuilder.build());

		// Get the HandlerThread's Looper and use it for the Handler.
		looper = thread.getLooper();
		handler = new ServiceHandler(looper);
	}

	// No binding provided, but needed. May have to look into this later.
	@Override
	public IBinder onBind(Intent intent) {
		return null;
	}

	// Runned *every time the startService() is called* from another class.
	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		Logger.i(tag, "Pendelum är awesome!");
		Message msg = handler.obtainMessage();
		msg.arg1 = startId;
		// Sets a flag saying that a trip has been started.
		SharedPrefsHandler.setActiveTrip(true);

		// Starts the location-service.
		initMaps();
		handler.sendMessage(msg);
		Logger.i(tag, "Service started!");
		startForeground(notificationID, notificationBuilder.build());
		return START_STICKY;
	}

	@Override
	public void onDestroy() {
		Logger.i(tag, "Service is done");
		SharedPrefsHandler.setActiveTrip(false);
		stopForeground(true);
	}

	// ==================== GOOGLE MAPS ==================== //
	/**
	 * Initializes all location-related components.
	 */
	private void initMaps() {
		// Connect to Google Services.
		locationClient = new LocationClient(getApplicationContext(), this, this);
		locationClient.connect();

		// Sign up for location update.
		if (isServicesConnected()) {
			Criteria criteria = new Criteria();
			criteria.setAccuracy(Criteria.ACCURACY_FINE);
			locationReq = LocationRequest.create();
			locationReq.setInterval(10000);
			locationReq.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
		} else {
			Toast.makeText(getApplicationContext(), R.string.not_connected_to_GCM, Toast.LENGTH_SHORT).show();
		}
	}

	/**
	 * Checks if connection to Google Play Services in possible.
	 * 
	 * @return true if connection is successful.
	 */
	private boolean isServicesConnected() {
		boolean isConnected = false;
		int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(getApplicationContext());
		if (ConnectionResult.SUCCESS == resultCode) {
			isConnected = true;
		} else {
			Dialog errorDialog = GooglePlayServicesUtil.getErrorDialog(resultCode, (Activity) getApplicationContext(), 9000);
			if (errorDialog != null) {
				errorDialog.show();
			}
			isConnected = false;
		}
		return isConnected;
	}

	@Override
	public void onConnected(Bundle connectionHint) {
		try {
			Thread.sleep(100); // TODO: Fulhack. This lets the device to finish the connection.
			locationClient.requestLocationUpdates(locationReq, this);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void onDisconnected() {
		notificationBuilder = new Notification.Builder(getApplicationContext()).setSmallIcon(R.drawable.ic_launcher)
		        .setContentTitle(getString(R.string.app_name)).setContentText(getString(R.string.gps_inactive));
		notificationManager.notify(notificationID, notificationBuilder.build());
		Toast.makeText(getApplicationContext(), getString(R.string.disconnected_from_gps), Toast.LENGTH_SHORT).show();
		locationClient.disconnect();
	}

	@Override
	public void onConnectionFailed(ConnectionResult result) {
		Toast.makeText(getApplicationContext(), getString(R.string.error_connecting_gps), Toast.LENGTH_SHORT).show();
	}

	@Override
	public void onLocationChanged(Location location) {
		Checkpoint cp = controller.getCurrentCheckpoint();
		if (cp != null) {
			
			if (SharedPrefsHandler.getDebugMode()) {
				lat = SharedPrefsHandler.getDebugLat();
				lng = SharedPrefsHandler.getDebugLng();
			} else {
				lat = location.getLatitude();
				lng = location.getLongitude();
			}

			boolean isUserOnSchedule = controller.isUserOnSchedule(cp.getTime());
			boolean isReached = controller.hasClosestCheckpointBeenReached(lat, lng, cp.getLat(), cp.getLng());
			boolean isLastCheckpointReached = controller.isLastCheckpointReached();

			// Not very proud if this chunk... :-|
			if (isLastCheckpointReached) {
				controller.userHasReachedFinalCheckpoint();
				Logger.i(tag, "User has reached its final checkpoint in time! Woohoo!");
				Toast.makeText(getApplicationContext(), R.string.goal_reached, Toast.LENGTH_SHORT).show();
				locationClient.disconnect();
				stopForeground(true);
				notificationBuilder = new Notification.Builder(getApplicationContext()).setSmallIcon(R.drawable.ic_launcher)
				        .setDefaults(Notification.FLAG_FOREGROUND_SERVICE).setContentTitle(getString(R.string.app_name))
				        .setContentText(getString(R.string.goal_reached));
				notificationManager.notify(notificationID, notificationBuilder.build());
			} else if (isUserOnSchedule && isReached) {
				controller.userHasReachedCheckpoint();
				Toast.makeText(getApplicationContext(), R.string.checkpoint_reached, Toast.LENGTH_SHORT).show();
				Logger.i(tag, getString(R.string.checkpoint_reached));
			} else if (isUserOnSchedule && !isReached) {
				notificationBuilder = new Notification.Builder(getApplicationContext()).setSmallIcon(R.drawable.ic_launcher)
				        .setDefaults(Notification.FLAG_FOREGROUND_SERVICE).setContentTitle(getString(R.string.app_name))
				        .setContentText(getString(R.string.user_is_in_time));
				notificationManager.notify(notificationID, notificationBuilder.build());
				Logger.i(tag, "User is in time, but has not reached its destination");
			} else if (!isUserOnSchedule) {
				Toast.makeText(getApplicationContext(), R.string.user_is_late, Toast.LENGTH_SHORT).show();
				Logger.i(tag, "User is late to its destination");
				SharedPrefsHandler.setActiveTrip(false);
				locationClient.disconnect();
				stopForeground(true);
				notificationBuilder = new Notification.Builder(getApplicationContext()).setSmallIcon(R.drawable.ic_launcher)
				        .setContentTitle(getString(R.string.app_name)).setContentText(getString(R.string.user_is_late));
				notificationManager.notify(notificationID, notificationBuilder.build());
			} else {
				Logger.i(tag, "Error in LocalService -> onLocationChanged()");
			}
		} else {
			Logger.i(tag, "No current checkpoint!");
		}
	}
	// ===================================================== //
}