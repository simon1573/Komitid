package se.komitid;

import se.controllers.CtrlrMain;
import se.controllers.CtrlrTime;
import se.fragments.FragCheckpointMap;
import se.fragments.FragFacebook;
import se.fragments.FragSetGoal;
import se.fragments.FragSettings;
import se.fragments.FragTime;
import se.fragments.FragTimeTable;
import se.others.ContextPasser;
import se.storage.DBAdapter;
import se.storage.Logger;
import se.storage.SharedPrefsHandler;
import android.app.Activity;
import android.app.DialogFragment;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

/**
 * Kom i tid. Choo choo!
 * 
 * version 0.9
 * 
 * @author Simon Cedergren <simon@tuxflux.se>
 * @author Fredrik Andersson <cfredrikandersson@gmail.com>
 */

public class MainActivity extends Activity {
	private final String tag = "Komitid | Mainactivity";
	private DBAdapter db;

	private FragTime fragTime;
	private FragSettings fragSettings;
	private FragSetGoal fragSetGoal;
	private FragCheckpointMap fragMap;
	private FragTimeTable fragTimeTable;
	private FragFacebook fragFacebook;
	private CtrlrTime ctrlrTime;
//	private CtrlrMain ctrlMain;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		SharedPrefsHandler.initialize(this);
		ContextPasser.setLocalContext(this);
		
		setContentView(R.layout.activity_main);
		db = new DBAdapter(this);
//		ctrlMain = new CtrlrMain();

		Logger.i("=====================");
		Logger.i("=====================");
		Logger.i("APPLIKATIONEN STARTAR");
		Logger.i("=====================");
		Logger.i("=====================");

		initFrags();
		checkForIntent();
		checkForRunningTrips();
		
//		if (!ctrlMain.isLocationActivated()) {
//			//TODO: Pop a dialog and tell user to turn on GPS, instead of a toast!
//			Toast.makeText(getApplicationContext(), "Sätt igång din GPS!", Toast.LENGTH_LONG).show();
//			Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
//		    startActivity(intent);
//		}
		
	}

	@Override
	protected void onResume() {
		super.onResume();

		if (SharedPrefsHandler.getAppState() != SharedPrefsHandler.STATE_IDLE)
			ctrlrTime.startTimeThread();
	}

	@Override
	protected void onPause() {
		super.onPause();
		ctrlrTime.stopTimeThread();
	}

	/**
	 * Resets the app to the default values if no trip is active in the
	 * background (running service).
	 */
	private void checkForRunningTrips() {
		if (!SharedPrefsHandler.tripIsActive()) {
			db.open();
			db.dropArrivals();
			db.dropDepartures();
			db.close();
			SharedPrefsHandler.restoreDefaultSettings();
		}
	}

	/**
	 * Initializes the fragments.
	 */
	public void initFrags() {
		fragTime = new FragTime();
		ctrlrTime = new CtrlrTime(fragTime, this);
		fragTime.setController(ctrlrTime);
		fragTimeTable = new FragTimeTable();
		fragSettings = new FragSettings();
		fragSetGoal = FragSetGoal.newInstance(this);
		fragMap = new FragCheckpointMap();
		fragFacebook = new FragFacebook();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		menu.clear();
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.action_settings: // Show settings
			switchFragment(fragSettings);
			break;
		case R.id.action_set_goal: // Show the "set goal"-screen
			switchFragment(fragSetGoal);
			break;
		case R.id.action_time_table: // Show the time table/timer
			switchFragment(fragTimeTable);
			break;
		case R.id.action_show_map: // Show the time table/timer
			switchFragment(fragMap);
			break;
		case R.id.action_time_info: // Show the time info
			switchFragment(fragTime);
			break;
		case R.id.action_facebook: // Show the Facebook login
			switchFragment(fragFacebook);
			break;
		case R.id.action_debug: // Sets the app in debug mode
			boolean debug = !SharedPrefsHandler.getDebugMode();
			Toast.makeText(this, getString(R.string.debug_mode)+ debug, Toast.LENGTH_SHORT).show();
			SharedPrefsHandler.setDebugMode(debug);
			break;
		case R.id.action_reset: // Reset settings (debug feature)
			SharedPrefsHandler.restoreDefaultSettings();
			db.open();
			db.dropArrivals();
			db.dropDepartures();
			db.close();
			break;
		}
		return super.onOptionsItemSelected(item);
	}

	/**
	 * Check if Activity was started by an intent, and if it contains any extras.
	 */
	public void checkForIntent() {
		Bundle extras = getIntent().getExtras();
		if (extras != null) {
			if (extras.getString("ALARM_RINGING") != null) {
				Log.i(tag, "Main received intent");
				SharedPrefsHandler.setAlarmRinging(true);
				SharedPrefsHandler.setActiveTrip(true);
				switchFragment(fragTime);
			}
		} else {
				switchFragment(fragSetGoal);
		}
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);

		// Ugly workaround in order to not use the support library...... :-|
		if (requestCode == 64206) {
			if (fragFacebook != null)
				fragFacebook.onActivityResult(requestCode, resultCode, data);
		}
	}

	/**
	 * Handles all that's needed to switch a fragment.
	 * 
	 * @param fragment
	 *            - the fragment class to switch to.
	 */
	public void switchFragment(Fragment fragment) {
		FragmentManager fm = getFragmentManager();
		FragmentTransaction ft = fm.beginTransaction();
		ft.addToBackStack(fragment.getTag());
		ft.replace(R.id.container_main, fragment);
		ft.commit();
	}

	/**
	 * Allows showing of DialogFragments.
	 * 
	 * @param newFragment
	 *            The fragment provided
	 */
	public void showDialogFragment(DialogFragment newFragment) {
		FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
		newFragment.show(fragmentTransaction, "dialog");
	}

	@Override
	protected void onNewIntent(Intent intent) {
		super.onNewIntent(intent);
		if (SharedPrefsHandler.tripIsActive()) {
			Log.i(tag, "onNewIntent");
			SharedPrefsHandler.setAlarmRinging(true);
			ctrlrTime.startTimeThread();
			switchFragment(fragTime);
		}
	}
}