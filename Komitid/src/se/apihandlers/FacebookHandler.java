package se.apihandlers;

import se.storage.Logger;
import android.os.Bundle;

import com.facebook.HttpMethod;
import com.facebook.Request;
import com.facebook.RequestAsyncTask;
import com.facebook.Session;

/**
 * The handler class for the Facebook connectivity. This class is mainly
 * responsible for posting status updates.
 * 
 * @author Fredrik Andersson <cfredrikandersson@gmail.com>
 * 
 */
public class FacebookHandler {

	private final String tag = "Komitid | FacebookHandler";

	/**
	 * Publishes the provided String as a Facebook status message, if a user is
	 * currently logged in.
	 * 
	 * @param message
	 *            The provided text to be posted as a status message.
	 */
	public void publishMessage(String message) {
		Session session = Session.getActiveSession();
		if (session != null && session.toString().contains("publish_actions")) {

			Bundle postParams = new Bundle();
			postParams.putString("name", "Komitid");
			postParams.putString("message", message);

			Request request = new Request(session, "me/feed", postParams, HttpMethod.POST, null);

			RequestAsyncTask task = new RequestAsyncTask(request);
			task.execute();
			Logger.i(tag, "Message sent to Facebook");
		} else {
			Logger.i(tag, "Message not sent to Facebook. User is not logged in.");
		}
	}
}
