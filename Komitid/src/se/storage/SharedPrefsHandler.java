package se.storage;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * This class provides an easy way to handle SharedPreferences.
 * 
 * @author Fredrik Andersson <cfredrikandersson@gmail.com>
 * @author Simon Cedergren <simon@tuxflux.se>
 */

public class SharedPrefsHandler {

	private static SharedPreferences prefs;

	private final static String PREFS_NAME = "se.komitid";

	private final static String PREPARATIONS = "PREPARATIONS";
	private final static String ENABLED_BUS = "ENABLED_BUS";
	private final static String ENABLED_TRAIN = "ENABLED_TRAIN";
	private final static String ENABLED_BOAT = "ENABLED_BOATS";
	private final static String WALKING_SPEED = "WALKING_SPEED";
	private final static String DEST_LATITUDE = "DEST_LATITUDE";
	private final static String DEST_LONGITUDE = "DEST_LONGITUDE";
	private final static String START_LATITUDE = "START_LATITUDE";
	private final static String START_LONGITUDE = "START_LONGITUDE";
	private final static String ARRIVAL_TIME = "ARRIVAL_TIME";
	private final static String SEGMENT = "SEGMENT";
	private final static String FIRST_RUN = "FIRST_RUN";
	private final static String ARRIVAL_HOUR = "ARRIVAL_HOUR";
	private final static String ARRIVAL_MINUTE = "ARRIVAL_MINUTE";
	private final static String ALARM_TIME = "ALARM_TIME";
	private final static String ALARM_RINGING = "ALARM_RINGING";
	private final static String ACTIVE_TRIP = "ACTIVE_TRIP";
	private final static String CURRENT_STATE = "CURRENT_STATE";
	private final static String HAS_BEEN_IN_TIME = "HAS_BEEN_IN_TIME";
	private final static String CURRENT_CHECKPOINT = "CURRENT_CHECKPOINT";
	private final static String DEBUG_LNG = "DEBUG_LNG";
	private final static String DEBUG_LAT = "DEBUG_LAT";
	private final static String DEBUG_MODE = "DEBUG_MODE";
	private final static String FACEBOOK_LOGIN_STATUS = "FACEBOOK_LOGIN_STATUS";

	public static final int STATE_IDLE = 0;
	public static final int STATE_ALARM = 1;
	public static final int STATE_PREP = 2;
	public static final int STATE_TRAVELING = 3;
	public static final int STATE_ARRIVED = 4;
	public static final int STATE_LATE = 5;

	/**
	 * Initializes the SharedPreferences-object.
	 * 
	 * @param context
	 *            The application context needed to get the preferences.
	 */
	public static void initialize(Context context) {
		prefs = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
	}

	/**
	 * Sets the time needed for preparations before departure.
	 * 
	 * @param timeMillis
	 *            The time needed (in milliseconds)
	 */
	public static void setPreparationTime(long timeMillis) {
		prefs.edit().putLong(PREPARATIONS, timeMillis).commit();

	}

	/**
	 * Returns the time needed for preparations before departure.
	 * 
	 * @return The time needed for preparations before departure.
	 */
	public static long getPreparationTime() {
		return prefs.getLong(PREPARATIONS, 0);
	}

	/**
	 * Sets a flag for whether or not buses should be included in the search.
	 * 
	 * @param enableBus
	 *            'true' if buses should be enabled, otherwise 'false'.
	 */
	public static void setEnableBus(boolean enableBus) {
		prefs.edit().putBoolean(ENABLED_BUS, enableBus).commit();
	}

	/**
	 * Returns the current setting for whether or not busses should be included
	 * in the search.
	 * 
	 * @return 'true' if enabled, otherwise 'false'.
	 */
	public static boolean getBusEnabled() {
		return prefs.getBoolean(ENABLED_BUS, true);
	}

	/**
	 * Sets a flag for whether or not trains should be included in the search.
	 * 
	 * @param enableTrain
	 *            'true' if trains should be enabled, otherwise 'false'.
	 */
	public static void setEnableTrain(boolean enableTrain) {
		prefs.edit().putBoolean(ENABLED_TRAIN, enableTrain).commit();
	}

	/**
	 * Returns the current setting for whether or not trains should be included
	 * in the search.
	 * 
	 * @return 'true' if enabled, otherwise 'false'.
	 */
	public static boolean getTrainEnabled() {
		return prefs.getBoolean(ENABLED_TRAIN, true);
	}

	/**
	 * Sets a flag for whether or not boats should be included in the search.
	 * 
	 * @param enableBoat
	 *            'true' if boats should be enabled, otherwise 'false'.
	 */
	public static void setEnableBoat(boolean enableBoats) {
		prefs.edit().putBoolean(ENABLED_BOAT, enableBoats).commit();

	}

	/**
	 * Returns the current setting for whether or not boats should be included
	 * in the search.
	 * 
	 * @return 'true' if enabled, otherwise 'false'.
	 */
	public static boolean getBoatEnabled() {
		return prefs.getBoolean(ENABLED_BOAT, true);
	}

	/**
	 * Sets the desired walking speed (km/h).
	 * 
	 * @param speed
	 *            The desired walking speed (km/h).
	 */
	public static void setWalkingSpeed(float speed) {
		prefs.edit().putFloat(WALKING_SPEED, speed).commit();
	}

	/**
	 * Returns the currently set desired walking speed.
	 * 
	 * @return The currently set desired walking speed (km/h).
	 */
	public static float getWalkingSpeed() {
		return prefs.getFloat(WALKING_SPEED, 5.5f);
	}

	/**
	 * Restores the settings to the default values.
	 */
	public static void restoreDefaultSettings() {
		setArrivalPickerHour(0);
		setArrivalPickerMinute(0);
		setDestLongitude(0);
		setDestLatitude(0);
		setStartLatitude(0);
		setStartLongitude(0);
		setArrivalTime(0);
		resetCurrentSegment();
		resetCurrentCheckpoint();
		setAlarmTime(0);
		setAlarmRinging(false);
		setAppState(0);
		setActiveTrip(false);
	}

	/**
	 * Sets a flag to indicate whether or not the app has been started before.
	 * 
	 * @param firstRun
	 *            Whether or not the app has been started before.
	 */
	public static void setFirstRun(boolean firstRun) {
		prefs.edit().putBoolean(FIRST_RUN, firstRun).commit();
	}

	/**
	 * Returns whether or not the app has been started before.
	 * 
	 * @return 'true' if first run, otherwise 'false'
	 */
	public static boolean isFirstRun() {
		return prefs.getBoolean(FIRST_RUN, true);
	}

	/**
	 * Stores the longitude of the chosen destination.
	 * 
	 * @param lng
	 *            The longtiude of the chosen destination
	 */
	public static void setDestLongitude(double lng) {
		prefs.edit().putString(DEST_LONGITUDE, "" + lng).commit();
	}

	/**
	 * Returns the longitude of the chosen destination
	 * 
	 * @return The longitude of the chosen destination
	 */
	public static double getDestLongitude() {
		return Double.parseDouble(prefs.getString(DEST_LONGITUDE, "0.0"));
	}

	/**
	 * Stores the latitude of the chosen destination
	 * 
	 * @param lat
	 *            The latitude of the chosen destination
	 */
	public static void setDestLatitude(double lat) {
		prefs.edit().putString(DEST_LATITUDE, "" + lat).commit();
	}

	/**
	 * Returns the latitude of the chosen destination
	 * 
	 * @return The latitude of the chosen destination
	 */
	public static double getDestLatitude() {
		return Double.parseDouble(prefs.getString(DEST_LATITUDE, "0.0"));
	}

	/**
	 * Stores the longitude of the starting location.
	 * 
	 * @param lng
	 *            The longtiude of the starting location
	 */
	public static void setStartLongitude(double lng) {
		prefs.edit().putString(START_LONGITUDE, "" + lng).commit();
	}

	/**
	 * Returns the longitude of the starting location
	 * 
	 * @return The longitude of the starting location
	 */
	public static double getStartLongitude() {
		return Double.parseDouble(prefs.getString(START_LONGITUDE, "0.0"));
	}

	/**
	 * Stores the latitude of the starting location
	 * 
	 * @param lat
	 *            The latitude of the starting location
	 */
	public static void setStartLatitude(double lat) {
		prefs.edit().putString(START_LATITUDE, "" + lat).commit();
	}

	/**
	 * Returns the latitude of the starting location
	 * 
	 * @return The latitude of the starting location
	 */
	public static double getStartLatitude() {
		return Double.parseDouble(prefs.getString(START_LATITUDE, "0.0"));
	}

	/**
	 * Stores the chosen time of arrival, in milliseconds
	 * 
	 * @param arrivalTime
	 *            The chosen time of arrival, in milliseconds
	 */
	public static void setArrivalTime(long arrivalTime) {
		prefs.edit().putLong(ARRIVAL_TIME, arrivalTime).commit();
	}

	/**
	 * Returns the chosen time of arrival, in milliseconds
	 * 
	 * @return The chosen time of arrival, in milliseconds
	 */
	public static long getArrivalTime() {
		return prefs.getLong(ARRIVAL_TIME, 0);
	}

	/**
	 * Stores the current trip segment by incrementing the previous value by 1.
	 */
	public static void addCurrentSegment() {
		int i = prefs.getInt(SEGMENT, 0);
		i++;
		prefs.edit().putInt(SEGMENT, i).commit();
	}

	/**
	 * Resets the value of the current segment.
	 */
	public static void resetCurrentSegment() {
		prefs.edit().putInt(SEGMENT, 0).commit();
	}

	/**
	 * Returns the current segment of the trip
	 * 
	 * @return The current segment of the trip
	 */
	public static int getCurrentSegment() {
		return prefs.getInt(SEGMENT, 0);
	}

	/**
	 * Returns the current checkpoint of the trip
	 * 
	 * @return The current checkpoint of the trip
	 */
	public static int getCurrentCheckpoint() {
		return prefs.getInt(CURRENT_CHECKPOINT, 0);
	}

	/**
	 * Stores the value of the current checkpoint by incrementing the previous
	 * value by 1.
	 */
	public static void addCurrentCheckpoint() {
		int i = prefs.getInt(CURRENT_CHECKPOINT, 0);
		i++;
		prefs.edit().putInt(CURRENT_CHECKPOINT, i).commit();
	}

	/**
	 * Resets the value of the current checkpoint.
	 */
	public static void resetCurrentCheckpoint() {
		prefs.edit().putInt(CURRENT_CHECKPOINT, 0).commit();
	}

	/**
	 * Stores the hour last selected using the time picker for arrival time.
	 * 
	 * @param hour
	 *            The hour last selected using the time picker for arrival time.
	 */
	public static void setArrivalPickerHour(int hour) {
		prefs.edit().putInt(ARRIVAL_HOUR, hour).commit();
	}

	/**
	 * Retrieves the hour last selected using the time picker for arrival time.
	 * 
	 * @return The hour last selected using the time picker for arrival time.
	 */
	public static int getArrivalPickerHour() {
		return prefs.getInt(ARRIVAL_HOUR, 0);
	}

	/**
	 * Stores the minute last selected using the time picker for arrival time.
	 * 
	 * @param minute
	 *            The minute last selected using the time picker for arrival
	 *            time.
	 */
	public static void setArrivalPickerMinute(int minute) {
		prefs.edit().putInt(ARRIVAL_MINUTE, minute).commit();
	}

	/**
	 * Retrieves the minute last selected using the time picker for arrival
	 * time.
	 * 
	 * @return The minute last selected using the time picker for arrival time.
	 */
	public static int getArrivalPickerMinute() {
		return prefs.getInt(ARRIVAL_MINUTE, 0);
	}

	/**
	 * Stores the currently set alarm time (milliseconds).
	 * 
	 * @param alarmTime
	 *            The currently set alarm time (milliseconds).
	 */
	public static void setAlarmTime(long alarmTime) {
		prefs.edit().putLong(ALARM_TIME, alarmTime).commit();
	}

	/**
	 * Returns the currently set alarm time
	 * 
	 * @return The currently set alarm time (milliseconds).
	 */
	public static long getAlarmTime() {
		return prefs.getLong(ALARM_TIME, 0);
	}

	/**
	 * Sets a flag for whether the alarm is ringing or not.
	 * 
	 * @param ringing
	 *            A flag indicating whether the alarm is ringing or not.
	 */
	public static void setAlarmRinging(boolean ringing) {
		prefs.edit().putBoolean(ALARM_RINGING, ringing).commit();
	}

	/**
	 * Returns the flag indicating whether the alarm is ringing or not.
	 * 
	 * @return 'true' if the alarm is ringing, otherwise 'false'.
	 */
	public static boolean isAlarmRinging() {
		return prefs.getBoolean(ALARM_RINGING, false);
	}

	/**
	 * Sets a flag indicating whether a trip is in progress or not.
	 * 
	 * @param isTripActive
	 *            A flag indicating whether a trip is in progress or not.
	 */
	public static void setActiveTrip(boolean isTripActive) {
		prefs.edit().putBoolean(ACTIVE_TRIP, isTripActive).commit();
	}

	/**
	 * Returns the flag indicating whether the alarm is ringing or not.
	 * 
	 * @return 'true' if a trip is in progress, otherwise 'false'.
	 */
	public static boolean tripIsActive() {
		return prefs.getBoolean(ACTIVE_TRIP, false);
	}

	/**
	 * Stores the number of times the user has been on time, by incrementing the
	 * previous value by 1.
	 */
	public static void addUserHasBeenOnTime() {
		int inTime = prefs.getInt(HAS_BEEN_IN_TIME, 0);
		inTime++;
		prefs.edit().putInt(HAS_BEEN_IN_TIME, inTime).commit();
	}

	/**
	 * Returns the number of times the user has been on time.
	 * 
	 * @return The number of times the user has been on time.
	 */
	public static int getUserHasBeenInTimeCount() {
		return prefs.getInt(HAS_BEEN_IN_TIME, 0);
	}

	/**
	 * Stores the spoofed debug-location latitude.
	 * 
	 * @param lat
	 *            The spoofed debug-location latitude.
	 */
	public static void setDebugLat(double lat) {
		prefs.edit().putString(DEBUG_LAT, "" + lat).commit();
	}

	/**
	 * Stores the spoofed debug-location longitude.
	 * 
	 * @param lat
	 *            The spoofed debug-location longitude.
	 */
	public static void setDebugLng(double lng) {
		prefs.edit().putString(DEBUG_LNG, "" + lng).commit();
	}

	/**
	 * Returns the spoofed debug-location latitude.
	 * 
	 * @return The spoofed debug-location latitude
	 */
	public static double getDebugLat() {
		return Double.parseDouble(prefs.getString(DEBUG_LAT, "0"));
	}

	/**
	 * Returns the spoofed debug-location longitude.
	 * 
	 * @return The spoofed debug-location longitude.
	 */
	public static double getDebugLng() {
		return Double.parseDouble(prefs.getString(DEBUG_LNG, "0"));
	}

	/**
	 * Returns a flag indicating whether the user is logged on to Facebook or
	 * not.
	 * 
	 * @return 'true' if logged in, otherwise 'false'.
	 */
	public static boolean getFacebookLoginStatus() {
		return prefs.getBoolean(FACEBOOK_LOGIN_STATUS, false);
	}

	/**
	 * Sets a flag indicating whether the user is logged on to Facebook or not.
	 * 
	 * @param status
	 *            A boolean indicating whether the user is logged on to Facebook
	 *            or not.
	 */
	public static void setFacebookLoginStatus(boolean status) {
		prefs.edit().putBoolean(FACEBOOK_LOGIN_STATUS, status).commit();
	}

	/**
	 * Sets the current state of the app (if an alarm is currently set and
	 * counting, if the user has progressed to "preparation-time", etc.).
	 * 
	 * @param state
	 *            The current "state" of the app.
	 */
	public static void setAppState(int state) {
		prefs.edit().putInt(CURRENT_STATE, state).commit();
	}

	/**
	 * Returns the current state of the app (if an alarm is currently set and
	 * counting, if the user has progressed to "preparation-time", etc.).
	 * 
	 * @return The current "state" of the app.
	 */
	public static int getAppState() {
		return prefs.getInt(CURRENT_STATE, 0);
	}

	/**
	 * Toggles debug mode.
	 * 
	 * @param debug
	 *            A boolean indicating whether debug mode should be enabled or
	 *            not.
	 */
	public static void setDebugMode(boolean debug) {
		prefs.edit().putBoolean(DEBUG_MODE, debug).commit();
	}

	/**
	 * Returns the flag for whether or not debug mode is enabled.
	 * 
	 * @return 'true' if debug mode is enabled, otherwise 'false'.
	 */
	public static boolean getDebugMode() {
		return prefs.getBoolean(DEBUG_MODE, false);
	}

}
