package se.storage;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;

import android.os.Environment;
import android.util.Log;

/**
 * A class that saves log output to a textfile, 
 * to ease the debugging when testing it "live".
 * 
 * @author Simon Cedergren <simon@tuxflux.se>
 */
public class Logger {
	
	public static void i(String log){
		i("", log);
	}
	
	public static void i(String tag, String log){
		try {
			File filename = new File(Environment.getExternalStorageDirectory().getPath() + "/komitid.log");
			FileOutputStream fos = new FileOutputStream(filename, true);
			OutputStreamWriter osw = new OutputStreamWriter(fos);
			osw.append(System.currentTimeMillis() + ": " + tag + " | " +log +"\n"); // Removed one extra space. Nöjd Andreas? :-)
			Log.i("Logger", "Wrote to log: " + log);
			osw.close();
			fos.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
